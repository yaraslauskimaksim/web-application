--
-- Table structure for table dw_users
--

CREATE TABLE may_users (
  Usr_Id            serial8                                   NOT NULL,
  Usr_Date_Added    TIMESTAMP WITHOUT TIME ZONE               NULL,
  Usr_Date_Modified TIMESTAMP WITHOUT TIME ZONE               NULL,
  Usr_First         VARCHAR(30)                               NULL,
  Usr_Is_Enabled    SMALLINT                                  NULL,
  Usr_Last          VARCHAR(30)                               NULL,
  Usr_Password      VARCHAR(60)                               NULL,
  Usr_Ref_UID       VARCHAR(36)                   UNIQUE      NULL,
  Usr_Username      VARCHAR(45)                   UNIQUE      NULL,
  Usr_Usr_Added     BIGINT                                    NULL,
  Usr_Usr_Modified  BIGINT                                    NULL,
  Usr_Email         VARCHAR(128)                              NOT NULL,
  PRIMARY KEY (Usr_Id),
  FOREIGN KEY (Usr_Usr_Modified) REFERENCES may_users (Usr_Id),
  FOREIGN KEY (Usr_Usr_Added) REFERENCES may_users (Usr_Id)
);

--
-- Dumping data for table dw_users
--

INSERT INTO may_users (Usr_Id,
                      Usr_Date_Added,
                      Usr_Date_Modified,
                      Usr_First,
                      Usr_Is_Enabled,
                      Usr_Last,
                      Usr_Password,
                      Usr_Ref_UID,
                      Usr_Username,
                      Usr_Usr_Added,
                      Usr_Usr_Modified,
                      Usr_Email)
VALUES (1,
  '2011-11-11 00:00:00',
  '2017-04-12 16:42:19',
  'Root',
  1,
  'Rootovsky',
  '$2a$10$Q1PVtE5KpqGuAAwomnmlt.dwMvcbmcxVe8HcN/Qe/rXLVKdHL2ZAy',
  'b624f23e-3556-41aa-9941-861edae1fd9c',
  'root',
  1,
  1,
  'root@gmail.com');

--
-- Table structure for table dw_user_role
--

CREATE TABLE may_user_role (
  Ur_Role   VARCHAR(255)        NOT NULL,
  Ur_Usr_Id BIGINT              NOT NULL,
  PRIMARY KEY (Ur_Role, Ur_Usr_Id),
  FOREIGN KEY (Ur_Usr_Id) REFERENCES may_users (Usr_Id)
);

--
-- Dumping data for table dw_user_role
--

INSERT INTO may_user_role (Ur_Role, Ur_Usr_Id)
VALUES ('ROLE_USER_READ', 1),
  ('ROLE_USER_WRITE', 1);
