import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/Home.vue'
import Login from '../components/login/Login.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '/#/',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        }
    ]
})
