import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router/router.js'
import Loading from './components/lib/Loading.vue'
import CenterContainer from './components/lib/CenterContainer.vue'

Vue.config.productionTip = false

Vue.use(Vuetify)

Vue.component('loading', Loading)
Vue.component('center-container', CenterContainer)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App },
})
