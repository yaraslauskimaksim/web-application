package com.myaraslauski.webapplication.component.security.extractor

import com.myaraslauski.webapplication.config.security.exception.InvalidJwtTokenException
import org.springframework.stereotype.Component

@Component
class JwtHeaderTokenExtractor : TokenExtractor {
    companion object {
        private const val HEADER_PREFIX = "Bearer "
    }

    override fun extract(payload: String): String {
        if (payload.isBlank()) {
            throw InvalidJwtTokenException("Authorization header cannot be blank.", Throwable())
        }
        if (payload.length < HEADER_PREFIX.length) {
            throw InvalidJwtTokenException("Invalid authorization header length.", Throwable())
        }
        return payload.substring(HEADER_PREFIX.length, payload.length)
    }
}
