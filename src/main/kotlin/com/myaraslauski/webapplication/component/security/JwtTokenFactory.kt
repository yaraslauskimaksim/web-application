package com.myaraslauski.webapplication.component.security

import com.myaraslauski.webapplication.config.security.JwtTokenProperties
import com.myaraslauski.webapplication.model.security.AccessJwtToken
import com.myaraslauski.webapplication.model.security.ApiUserDetails
import com.myaraslauski.webapplication.model.security.JwtToken
import com.myaraslauski.webapplication.model.security.Scopes
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtTokenFactory(private val jwtTokenProperties: JwtTokenProperties) {
    private val key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtTokenProperties.signKey))

    fun createAccessJwtToken(userDetails: UserDetails): JwtToken {
        val claims = Jwts.claims().setSubject(userDetails.username)
        claims["scopes"] = userDetails.authorities.map(GrantedAuthority::toString)
        if (ApiUserDetails::class.java.isAssignableFrom(userDetails.javaClass)) {
            claims["user"] = ApiUserDetails::class.java.cast(userDetails).userId
        }
        return createToken(userDetails, claims, jwtTokenProperties.expirationTimeInMinutes)
    }

    fun createRefreshToken(userDetails: UserDetails, uid: String): JwtToken {
        val claims = Jwts.claims().setSubject(userDetails.username)
        claims["scopes"] = listOf(Scopes.REFRESH_TOKEN.authority())
        return createToken(userDetails, claims, jwtTokenProperties.refreshExpirationTimeInMinutes, uid)
    }

    private fun createToken(
        userDetails: UserDetails,
        claims: Claims,
        expirationTimeInMinutes: Int,
        uid: String? = null
    ): AccessJwtToken {
        if (userDetails.username.isBlank()) {
            throw IllegalArgumentException("Cannot create JWT Token without username")
        }

        val calendar = Calendar.getInstance()
        val currentTime = calendar.time

        calendar.add(Calendar.MINUTE, expirationTimeInMinutes)
        val expiration = calendar.time


        val token = Jwts.builder()
            .setClaims(claims)
            .setIssuer(jwtTokenProperties.issuer)
            .apply { if (uid != null) setId(uid) }
            .setIssuedAt(currentTime)
            .setExpiration(expiration)
            .signWith(key)
            .compact()

        return AccessJwtToken(token, claims)
    }
}
