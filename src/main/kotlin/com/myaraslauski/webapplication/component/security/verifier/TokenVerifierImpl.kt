package com.myaraslauski.webapplication.component.security.verifier

import com.myaraslauski.webapplication.service.security.UserService
import org.springframework.stereotype.Component

@Component
class TokenVerifierImpl(private val userService: UserService) : TokenVerifier {

    override fun isValid(jti: String?) = userService.isValidRefreshTokenUID(jti)
}
