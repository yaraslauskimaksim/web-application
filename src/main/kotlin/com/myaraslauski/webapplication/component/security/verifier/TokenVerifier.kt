package com.myaraslauski.webapplication.component.security.verifier

interface TokenVerifier {
    fun isValid(jti: String?): Boolean
}
