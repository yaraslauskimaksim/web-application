package com.myaraslauski.webapplication.component.security.extractor

interface TokenExtractor {
    fun extract(payload: String): String
}
