package com.myaraslauski.webapplication.component.security.provider

import com.myaraslauski.webapplication.config.security.JwtTokenProperties
import com.myaraslauski.webapplication.model.security.JwtAuthenticationToken
import com.myaraslauski.webapplication.model.security.JwtToken
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component

@Component
class JwtAuthenticationProvider(private val jwtTokenProperties: JwtTokenProperties) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        val token = JwtToken::class.java.cast(authentication.credentials)

        val jwsClaims = token.getClaims(jwtTokenProperties.signKey)
        val subject = jwsClaims?.subject
        @Suppress("UNCHECKED_CAST")
        val scopes = jwsClaims?.get("scopes", List::class.java) as List<String>
        val userId = jwsClaims.get("user", Int::class.javaObjectType)

        val authorities = scopes.map(::SimpleGrantedAuthority)
        return JwtAuthenticationToken(subject, userId?.toLong(), authorities)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return JwtAuthenticationToken::class.java.isAssignableFrom(authentication)
    }
}
