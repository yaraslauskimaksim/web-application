package com.myaraslauski.webapplication.component.security

import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.OrRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.http.HttpServletRequest

class SkipPathAndMethodsRequestMatcher(
    pathsToSkip: List<String>,
    private val methodsToSkip: List<String>,
    processingPaths: List<String>
) : RequestMatcher {
    private val skipMatcher = OrRequestMatcher(buildMatchers(pathsToSkip))
    private val processingMatcher = OrRequestMatcher(buildMatchers(processingPaths))

    private fun buildMatchers(paths: List<String>) = paths.map(::AntPathRequestMatcher)

    override fun matches(request: HttpServletRequest): Boolean {
        return !skipMatcher.matches(request)
                && !methodsToSkip.contains(request.method)
                && processingMatcher.matches(request)
    }
}
