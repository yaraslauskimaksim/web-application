package com.myaraslauski.webapplication.model.security

import io.jsonwebtoken.Claims

interface JwtToken {
    fun getToken(): String?

    fun getClaims(signingKey: String): Claims?
}