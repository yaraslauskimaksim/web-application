package com.myaraslauski.webapplication.model.security

import org.springframework.security.core.GrantedAuthority

enum class Role : GrantedAuthority {

    //user management
    ROLE_USER_READ,
    ROLE_USER_WRITE,

    //permissions management
    ROLE_PERMISSION_GROUP_WRITE,
    ROLE_PERMISSION_GROUP_READ,
    ROLE_PERMISSION_GROUP_DELETE;

    fun hasRole() = "hasRole('$this')"

    override fun getAuthority() = name
}