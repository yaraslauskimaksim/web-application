package com.myaraslauski.webapplication.model.security

import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
annotation class RoleSecured(vararg val value: Role)