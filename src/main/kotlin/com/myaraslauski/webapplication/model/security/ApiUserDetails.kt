package com.myaraslauski.webapplication.model.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User

class ApiUserDetails(
    username: String,
    val userId: Long?,
    password: String,
    enabled: Boolean,
    accountNonExpired: Boolean,
    credentialsNonExpired: Boolean,
    accountNonLocked: Boolean,
    authorities: Collection<GrantedAuthority>
) : User(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities)
