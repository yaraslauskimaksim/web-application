package com.myaraslauski.webapplication.model.security

import com.myaraslauski.webapplication.config.security.exception.InvalidJwtTokenException
import com.myaraslauski.webapplication.config.security.exception.JwtExpiredTokenException
import io.jsonwebtoken.*
import io.jsonwebtoken.security.SecurityException
import org.slf4j.LoggerFactory

class RawAccessJwtToken(private val token: String?) : JwtToken {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getToken() = token

    override fun getClaims(signingKey: String): Claims {
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).body
        } catch (ex: UnsupportedJwtException) {
            logger.debug("Invalid JWT Token")
            throw InvalidJwtTokenException(ex)
        } catch (ex: MalformedJwtException) {
            logger.debug("Invalid JWT Token")
            throw InvalidJwtTokenException(ex)
        } catch (ex: IllegalArgumentException) {
            logger.debug("Invalid JWT Token")
            throw InvalidJwtTokenException(ex)
        } catch (ex: SecurityException) {
            logger.debug("Invalid JWT Token")
            throw InvalidJwtTokenException(ex)
        } catch (expiredEx: ExpiredJwtException) {
            logger.debug("JWT Token is expired")
            throw JwtExpiredTokenException("JWT Token expired", expiredEx)
        }
    }
}