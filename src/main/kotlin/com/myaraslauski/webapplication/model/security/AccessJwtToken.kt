package com.myaraslauski.webapplication.model.security

import com.fasterxml.jackson.annotation.JsonIgnore
import io.jsonwebtoken.Claims

class AccessJwtToken(
    private val rawToken: String?,
    @JsonIgnore private val claims: Claims?
) : JwtToken {

    override fun getToken() = rawToken

    override fun getClaims(signingKey: String) = claims
}
