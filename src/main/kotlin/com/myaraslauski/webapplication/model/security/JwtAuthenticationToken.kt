package com.myaraslauski.webapplication.model.security

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class JwtAuthenticationToken : AbstractAuthenticationToken {
    private var jwtToken: JwtToken? = null
    private val username: String?
    val userId: Long?

    constructor(jwtToken: JwtToken?) : super(null) {
        this.jwtToken = jwtToken
        username = null
        userId = null
        isAuthenticated = false
    }

    constructor(username: String?, userId: Long?, authorities: Collection<GrantedAuthority>?) : super(authorities) {
        eraseCredentials()
        this.username = username
        this.userId = userId
        super.setAuthenticated(true)
    }

    override fun getCredentials() = jwtToken

    override fun getPrincipal() = username

    override fun eraseCredentials() {
        super.eraseCredentials()
        jwtToken = null
    }

    override fun setAuthenticated(authenticated: Boolean) {
        if (authenticated) {
            throw IllegalArgumentException("Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead")
        }
        super.setAuthenticated(false)
    }
}