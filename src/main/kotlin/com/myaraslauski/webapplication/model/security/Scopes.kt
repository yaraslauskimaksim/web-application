package com.myaraslauski.webapplication.model.security

enum class Scopes {
    REFRESH_TOKEN;

    fun authority() = "ROLE_$name"
}