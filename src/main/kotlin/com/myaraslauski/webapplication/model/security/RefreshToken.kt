package com.myaraslauski.webapplication.model.security

import com.myaraslauski.webapplication.config.security.exception.InvalidJwtTokenException


class RefreshToken(token: JwtToken, signingKey: String) {
    private val claims = token.getClaims(signingKey)
    val jti = claims?.id
    val subject = claims?.subject

    init {
        @Suppress("UNCHECKED_CAST")
        val scopes = claims?.get("scopes", List::class.java) as List<String>?
        scopes?.let(this::checkScope) ?: throw InvalidJwtTokenException(Throwable())
    }

    private fun checkScope(scopes: List<String>) {
        val expectedScope = Scopes.REFRESH_TOKEN.authority()
        scopes.firstOrNull { expectedScope == it } ?: throw InvalidJwtTokenException(Throwable())
    }
}