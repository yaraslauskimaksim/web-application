package com.myaraslauski.webapplication.model.security

class LoginRequest(
    username: String,
    val password: String
) {
    val username = username.toLowerCase()
}
