package com.myaraslauski.webapplication.model

inline fun <reified T : Any?> T?.notNull() = this as T