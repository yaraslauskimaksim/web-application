package com.myaraslauski.webapplication.model.email

class EmailMessage constructor(
    val to: List<String>,
    subject: String?,
    body: String?,
    val attachments: Map<String, ByteArray> = emptyMap()
) {
    val subject = subject ?: ""
    val body = body ?: ""
}