package com.myaraslauski.webapplication.model.user

import org.hibernate.validator.constraints.Email
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class UserProfile(
    @get: Email(message = "Invalid email")
    val email: String? = null,

    @get: Pattern(
        regexp = "^[A-Z][a-z]{1,29}$",
        message = "First letter must be capital. Size must be between 2 and 30 "
    )
    val first: String? = null,

    @get: Pattern(
        regexp = "^[A-Z][a-z]{1,29}$",
        message = "First letter must be capital. Size must be between 2 and 30 "
    )
    val last: String? = null,

    @get: Size(max = 60)
    val password: String? = null
)
