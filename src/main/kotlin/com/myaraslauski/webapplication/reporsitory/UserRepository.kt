package com.myaraslauski.webapplication.reporsitory

import com.myaraslauski.webapplication.entity.User
import com.myaraslauski.webapplication.model.security.Role
import com.myaraslauski.webapplication.model.security.RoleSecured
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource
import java.util.*

@RepositoryRestResource
interface UserRepository : PagingAndSortingRepository<User, Long> {
    @RoleSecured(Role.ROLE_USER_READ)
    override fun findAll(sort: Sort): Iterable<User>

    @RoleSecured(Role.ROLE_USER_READ)
    override fun findAll(pageable: Pageable): Page<User>

    @RoleSecured(Role.ROLE_USER_READ)
    override fun findAll(): Iterable<User>

    @RoleSecured(Role.ROLE_USER_READ)
    override fun findById(@Param("id") id: Long): Optional<User>

    @RestResource(exported = false)
    fun findByEmail(email: String): User?

    @Query("select u from User u where u.username = ?#{principal}")
    fun currentlyLogged(): User

    @RoleSecured(Role.ROLE_USER_READ)
    override fun count(): Long

    @Suppress("FINAL_UPPER_BOUND")
    @RoleSecured(Role.ROLE_USER_WRITE)
    override fun <S : User> save(entity: S): S

    @RestResource(exported = false)
    @Query("SELECT u FROM User u LEFT JOIN FETCH u.roles WHERE u.id = (:id)")
    fun findOneWithRoles(@Param("id") id: Long): User

    @RestResource(exported = false)
    fun findByUsername(username: String): User?

    @RestResource(exported = false)
    fun countByRefreshTokenUID(uid: String): Long

    @Modifying
    @Query("update User u set u.refreshTokenUID =:uid where u.username =:username")
    @RestResource(exported = false)
    fun updateRefreshTokenUID(@Param("username") username: String, @Param("uid") uid: String)

    @Modifying
    @Query("update User u set u.refreshTokenUID = NULL where u.username =:username")
    @RestResource(exported = false)
    fun removeTokenUIDFor(@Param("username") username: String)

    @Modifying
    @Query("update User u set u.password =:newPassword, u.refreshTokenUID = NULL where u.username =:username")
    @RestResource(exported = false)
    fun resetPassword(@Param("username") username: String, @Param("newPassword") newPassword: String)

    @Modifying
    @Query("update User u set u.email =:email, u.first =:first, u.last =:last, u.password =:password where u.username = ?#{principal}")
    @RestResource(exported = false)
    fun updateProfile(
        @Param("email") email: String, @Param("first") first: String, @Param("last") last: String, @Param(
            "password"
        ) password: String
    )

    @Query(
        "select u from User u" +
                " left join u.roles r" +
                " where (u.username =:username or :username is null)" +
                " and (u.first =:firstName or :firstName is null) " +
                " and (u.last =:lastName or :lastName is null)" +
                " and (r in :roleName or :roleName is null)" +
                " group by u"
    )
    fun search(
        @Param("username") username: String?,
        @Param("firstName") first: String?,
        @Param("lastName") last: String?,
        @Param("roleName") role: Role?,
        pageable: Pageable
    ): Page<User>

}
