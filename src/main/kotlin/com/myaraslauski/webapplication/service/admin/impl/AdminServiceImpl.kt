package com.myaraslauski.webapplication.service.admin.impl

import com.myaraslauski.webapplication.model.security.Role
import com.myaraslauski.webapplication.service.admin.AdminService
import org.springframework.stereotype.Service

@Service
open class AdminServiceImpl : AdminService {
    override fun listRoles() = Role.values().toList()
}
