package com.myaraslauski.webapplication.service.admin

import com.myaraslauski.webapplication.model.security.Role

interface AdminService {

    fun listRoles(): List<Role>
}
