package com.myaraslauski.webapplication.service.email.impl

import com.myaraslauski.webapplication.model.email.EmailMessage
import com.myaraslauski.webapplication.service.email.EmailService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ByteArrayResource
import org.springframework.mail.MailSendException
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service

@Service
@Profile("vault")
class EmailServiceImpl(
    private val javaMailSender: JavaMailSender,
    @param:Value("\${spring.mail.from}") private val from: String
) : EmailService {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun sendHtmlMessage(emailMessage: EmailMessage) = sendMessage(emailMessage) {
        it.setText(emailMessage.body, true)
    }

    override fun sendMessage(emailMessage: EmailMessage) = sendMessage(emailMessage) {
        it.setText(emailMessage.body)
    }

    private fun sendMessage(emailMessage: EmailMessage, setText: (MimeMessageHelper) -> Unit) {
        val message = javaMailSender.createMimeMessage()
        val helper = MimeMessageHelper(message, true)
        with(helper) {
            setFrom(from)
            setTo(emailMessage.to.toTypedArray())
            setSubject(emailMessage.subject)
            setText(this)
            emailMessage.attachments.forEach { addAttachment(it.key, ByteArrayResource(it.value)) }
        }
        try {
            javaMailSender.send(message)
            logger.debug("Send email to ${emailMessage.to} with subject ${emailMessage.subject}")
        } catch (exception: MailSendException) {
            logger.error(
                "Exception on sending email to ${emailMessage.to} with subject ${emailMessage.subject}",
                exception
            )
        }
    }
}
