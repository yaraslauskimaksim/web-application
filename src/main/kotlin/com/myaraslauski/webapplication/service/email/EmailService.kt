package com.myaraslauski.webapplication.service.email

import com.myaraslauski.webapplication.model.email.EmailMessage

interface EmailService {
    fun sendMessage(emailMessage: EmailMessage)
    fun sendHtmlMessage(emailMessage: EmailMessage)
}
