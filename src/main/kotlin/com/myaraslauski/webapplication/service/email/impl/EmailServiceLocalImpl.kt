package com.myaraslauski.webapplication.service.email.impl

import com.myaraslauski.webapplication.model.email.EmailMessage
import com.myaraslauski.webapplication.service.email.EmailService
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

@Service
@Profile("!vault")
class EmailServiceLocalImpl : EmailService {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun sendMessage(emailMessage: EmailMessage) = send(emailMessage)

    override fun sendHtmlMessage(emailMessage: EmailMessage) = send(emailMessage)

    private fun send(emailMessage: EmailMessage) {
        logger.info("Send email to {} with subject {}", emailMessage.to, emailMessage.subject)
    }
}
