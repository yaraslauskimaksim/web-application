package com.myaraslauski.webapplication.service.security.impl

import com.myaraslauski.webapplication.model.email.EmailMessage
import com.myaraslauski.webapplication.model.notNull
import com.myaraslauski.webapplication.model.user.UserProfile
import com.myaraslauski.webapplication.reporsitory.UserRepository
import com.myaraslauski.webapplication.service.email.EmailService
import com.myaraslauski.webapplication.service.security.UserService
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
open class UserServiceImpl(
        private val userRepository: UserRepository,
        private val emailService: EmailService,
        private val passwordEncoder: PasswordEncoder
) : UserService {
    companion object {
        private const val EMAIL_SUBJECT = "Reset password"
        private const val EMAIL_BODY_FORMAT =
            "Your password has been reset\nNew password is %s\nDon't forget to change password in you profile."
    }

    @Throws(Exception::class)
    override fun resetPassword(emailOrLogin: String) {
        val user = if (emailOrLogin.contains("@")) {
            userRepository.findByEmail(emailOrLogin)
        } else {
            userRepository.findByUsername(emailOrLogin)
        }

        user ?: throw Exception("User not found")
        if (user.email.isEmpty()) {
            throw Exception("You didn't provide an email. Contact administrator.")
        }

        val newPassword = RandomStringUtils.randomAlphanumeric(12)
        userRepository.resetPassword(user.username, passwordEncoder.encode(newPassword))
        emailService.sendMessage(
            EmailMessage(
                listOf(user.email),
                EMAIL_SUBJECT,
                String.format(EMAIL_BODY_FORMAT, newPassword)
            )
        )
    }

    override fun saveRefreshTokenUID(username: String, uid: String) {
        userRepository.updateRefreshTokenUID(username, uid)
    }

    override fun isValidRefreshTokenUID(uid: String?): Boolean {
        return uid != null && userRepository.countByRefreshTokenUID(uid) == 1L
    }

    override fun removeTokenUIDFor(username: String) {
        userRepository.removeTokenUIDFor(username)
    }

    override fun updateProfile(userProfile: UserProfile) {
        val currentUser = userRepository.currentlyLogged()
        with(currentUser) {
            email = userProfile.email ?: ""
            first = userProfile.first ?: ""
            last = userProfile.last ?: ""
            if (!userProfile.password.isNullOrBlank()) {
                password = userProfile.password.notNull()
            }
        }
        userRepository.updateProfile(currentUser.email, currentUser.first, currentUser.last, currentUser.password)
    }
}
