package com.myaraslauski.webapplication.service.security.impl

import com.myaraslauski.webapplication.config.security.JwtTokenProperties
import com.myaraslauski.webapplication.model.security.RawAccessJwtToken
import com.myaraslauski.webapplication.service.security.JWTService
import org.springframework.stereotype.Service

@Service
class JWTServiceImpl(private val settings: JwtTokenProperties) : JWTService {

    override fun getUsername(token: String): String {
        val rawAccessJwtToken = RawAccessJwtToken(token)
        val jws = rawAccessJwtToken.getClaims(settings.signKey)
        return jws.get("sub", String::class.java)
    }
}
