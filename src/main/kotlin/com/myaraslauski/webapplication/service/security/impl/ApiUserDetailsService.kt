package com.myaraslauski.webapplication.service.security.impl

import com.myaraslauski.webapplication.model.security.ApiUserDetails
import com.myaraslauski.webapplication.model.security.Role
import com.myaraslauski.webapplication.reporsitory.UserRepository
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class ApiUserDetailsService(private val userRepository: UserRepository) : UserDetailsService {

    @Transactional(readOnly = true)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username)
            ?: throw BadCredentialsException("Username or Password not valid.")

        val roles = mutableSetOf<Role>().apply {
            addAll(user.roles)
        }

        return ApiUserDetails(
            user.username,
            user.id,
            user.password,
            user.enabled,
            true,
            true,
            true,
            roles
        )
    }
}
