package com.myaraslauski.webapplication.service.security

interface JWTService {
    fun getUsername(token: String): String
}
