package com.myaraslauski.webapplication.service.security.impl

import com.myaraslauski.webapplication.model.security.JwtAuthenticationToken
import com.myaraslauski.webapplication.service.security.SecurityContextService
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class SecurityContextServiceImpl : SecurityContextService {
    override fun currentlyLoggedUserId(): Long {
        return SecurityContextHolder.getContext()?.authentication?.let { it as JwtAuthenticationToken }?.userId ?: 1L
    }

    override fun currentlyLoggedUserAuthorities(): Collection<GrantedAuthority> {
        return SecurityContextHolder.getContext()?.authentication?.let { it as JwtAuthenticationToken }?.authorities
            ?: emptyList()
    }

    override fun currentlyLoggedUserName(): String {
        return SecurityContextHolder.getContext()?.authentication?.let { it as JwtAuthenticationToken }?.name ?: "root"
    }
}
