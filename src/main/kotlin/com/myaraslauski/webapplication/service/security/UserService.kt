package com.myaraslauski.webapplication.service.security

import com.myaraslauski.webapplication.model.user.UserProfile

interface UserService {
    fun resetPassword(emailOrLogin: String)

    fun saveRefreshTokenUID(username: String, uid: String)

    fun isValidRefreshTokenUID(uid: String?): Boolean

    fun removeTokenUIDFor(username: String)

    fun updateProfile(userProfile: UserProfile)
}
