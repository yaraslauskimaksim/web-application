package com.myaraslauski.webapplication.service.security

import org.springframework.security.core.GrantedAuthority

interface SecurityContextService {
    fun currentlyLoggedUserId(): Long

    fun currentlyLoggedUserAuthorities(): Collection<GrantedAuthority>

    fun currentlyLoggedUserName(): String
}
