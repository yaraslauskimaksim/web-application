package com.myaraslauski.webapplication.config.db

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import javax.sql.DataSource

@Configuration
@Profile("!test")
open class DBConfig {
    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    open fun commonDataSource(): DataSource = DataSourceBuilder.create().type(HikariDataSource::class.java).build()


    @Bean
    open fun namedParameterJdbcTemplate(@Qualifier("commonDataSource") commonDataSource: DataSource) =
            NamedParameterJdbcTemplate(commonDataSource).apply {
                (jdbcOperations as JdbcTemplate).fetchSize = Int.MIN_VALUE
            }
}