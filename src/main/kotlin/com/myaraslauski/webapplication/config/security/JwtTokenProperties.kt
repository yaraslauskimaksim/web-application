package com.myaraslauski.webapplication.config.security

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("jwt.token")
class JwtTokenProperties {
    var expirationTimeInMinutes = 10
    var issuer: String = ""
    var signKey: String = ""
    var refreshExpirationTimeInMinutes = 1440
}