package com.itsupportme.datawarehouse.config.security.exception

import org.springframework.security.authentication.AuthenticationServiceException

class AuthMethodNotSupportedException(msg: String) : AuthenticationServiceException(msg)