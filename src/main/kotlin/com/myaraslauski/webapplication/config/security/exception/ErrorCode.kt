package com.myaraslauski.webapplication.config.security.exception

import com.itsupportme.datawarehouse.config.security.exception.AuthMethodNotSupportedException
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.BadCredentialsException

enum class ErrorCode constructor(
    private val errorCode: Int,
    private val status: HttpStatus,
    private val message: String?,
    private val exceptions: Array<Class<out Exception>> = emptyArray()
) {
    AUTHENTICATION_FAILED(
        1,
        HttpStatus.UNAUTHORIZED,
        "Authentication failed",
        arrayOf(AuthMethodNotSupportedException::class.java, InvalidJwtTokenException::class.java)
    ),
    JWT_TOKEN_EXPIRED(2, HttpStatus.UNAUTHORIZED, "Token has expired", arrayOf(JwtExpiredTokenException::class.java)),
    BAD_CREDENTIALS(
        3,
        HttpStatus.BAD_REQUEST,
        "Username or Password not valid",
        arrayOf(BadCredentialsException::class.java)
    ),
    NOT_VALID_JSON(
        4,
        HttpStatus.BAD_REQUEST,
        "Content does not conform to JSON syntax",
        arrayOf(NotValidJsonException::class.java)
    ),
    REQUIRED_FIELD_EMPTY(5, HttpStatus.BAD_REQUEST, null, arrayOf(RequiredFieldEmptyException::class.java)),
    ACCESS_DENIED(
        6,
        HttpStatus.FORBIDDEN,
        "You don't have permission to access",
        arrayOf(AccessDeniedException::class.java)
    ),
    UNEXPECTED_ERROR(100, HttpStatus.INTERNAL_SERVER_ERROR, null);

    fun message(ex: Exception) = message ?: ex.message

    fun exceptions() = exceptions

    fun status() = status

    fun errorCode() = errorCode
}
