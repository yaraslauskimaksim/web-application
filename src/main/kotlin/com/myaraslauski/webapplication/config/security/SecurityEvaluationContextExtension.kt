package com.myaraslauski.webapplication.config.security

import org.springframework.data.repository.query.spi.EvaluationContextExtensionSupport
import org.springframework.security.access.expression.SecurityExpressionRoot
import org.springframework.security.core.context.SecurityContextHolder

class SecurityEvaluationContextExtension : EvaluationContextExtensionSupport() {

    override fun getExtensionId() = "security"

    override fun getRootObject(): SecurityExpressionRoot {
        return object : SecurityExpressionRoot(SecurityContextHolder.getContext().authentication) {
        }
    }
}