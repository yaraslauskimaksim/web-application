package com.myaraslauski.webapplication.config.security.exception

import java.time.LocalDate

class ErrorResponse private constructor(
    exception: Exception,
    code: ErrorCode
) {
    val status: Int = code.status().value()
    val message: String? = code.message(exception)
    val errorCode: Int = code.errorCode()
    val timestamp: LocalDate = LocalDate.now()

    constructor(exception: Exception) : this(
        exception,
        ErrorCode.values().firstOrNull { it.exceptions().any { exception.javaClass == it } }
            ?: ErrorCode.UNEXPECTED_ERROR)
}
