package com.myaraslauski.webapplication.config.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.myaraslauski.webapplication.component.security.SkipPathAndMethodsRequestMatcher
import com.myaraslauski.webapplication.component.security.extractor.TokenExtractor
import com.myaraslauski.webapplication.web.filter.AjaxLoginProcessingFilter
import com.myaraslauski.webapplication.web.filter.JwtTokenAuthenticationProcessingFilter
import com.myaraslauski.webapplication.web.handler.ErrorHandler
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.repository.query.spi.EvaluationContextExtensionSupport
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.Filter

@Configuration
@EnableConfigurationProperties(JwtTokenProperties::class)
@EnableWebSecurity
open class WebSecurityConfig(
        private val jwtAccessDeniedHandler: AccessDeniedHandler,
        private val jwtAuthenticationEntryPoint: AuthenticationEntryPoint,
        private val successHandler: AuthenticationSuccessHandler,
        private val errorHandler: ErrorHandler,
        private val objectMapper: ObjectMapper,
        private val tokenExtractor: TokenExtractor,
        private val ajaxAuthenticationProvider: AuthenticationProvider,
        private val jwtAuthenticationProvider: AuthenticationProvider
) : WebSecurityConfigurerAdapter() {

    companion object {
        private const val TOKEN_BASED_AUTH_ENTRY_INTERNAL_POINT = "/api/internal/**"
        private const val FORM_BASED_LOGIN_ENTRY_POINT = "/api/auth/login"
        private const val TOKEN_REFRESH_ENTRY_POINT = "/api/auth/token"
        private const val TOKEN_BASED_AUTH_ENTRY_POINT = "/api/**"
        private const val ALL_PATHS_PATTERN = "/**"
        private const val SERVER_STATUS_PATHS_PATTERN = "/status/**"
        private const val LIVE_DATA_STATISTICS_PATHS_PATTERN = "/livedata/statistics"
    }

    override fun configure(http: HttpSecurity) {
        http
            .cors()
            .disable()
            .csrf()
            .disable()
            .exceptionHandling()
            .accessDeniedHandler(jwtAccessDeniedHandler)
            .authenticationEntryPoint(jwtAuthenticationEntryPoint)
            .and().sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and().authorizeRequests()
            .antMatchers(FORM_BASED_LOGIN_ENTRY_POINT, TOKEN_REFRESH_ENTRY_POINT).permitAll()
            .and().authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, ALL_PATHS_PATTERN).permitAll()
//            .antMatchers(SERVER_STATUS_PATHS_PATTERN).access(Role.ROLE_SERVER_STATUS.hasRole())
            .antMatchers(LIVE_DATA_STATISTICS_PATHS_PATTERN, TOKEN_BASED_AUTH_ENTRY_INTERNAL_POINT).authenticated()
            .and()
            .addFilterBefore(ajaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter::class.java)
            .addFilterBefore(jwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }


    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(ajaxAuthenticationProvider)
        auth.authenticationProvider(jwtAuthenticationProvider)
    }

    @Bean
    open fun jwtTokenAuthenticationProcessingFilter(): Filter {
        val pathsToSkip = listOf(TOKEN_REFRESH_ENTRY_POINT, FORM_BASED_LOGIN_ENTRY_POINT)
        val methodsToSkip = listOf(HttpMethod.OPTIONS.name)
        val processingPaths =
            listOf(TOKEN_BASED_AUTH_ENTRY_POINT, SERVER_STATUS_PATHS_PATTERN, LIVE_DATA_STATISTICS_PATHS_PATTERN)
        val matcher = SkipPathAndMethodsRequestMatcher(pathsToSkip, methodsToSkip, processingPaths)

        val filter = JwtTokenAuthenticationProcessingFilter(errorHandler, tokenExtractor, matcher)

        filter.setAuthenticationManager(authenticationManagerBean())
        return filter
    }

    @Bean
    open fun ajaxLoginProcessingFilter(): Filter {
        val filter = AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT, successHandler, errorHandler, objectMapper)
        filter.setAuthenticationManager(authenticationManagerBean())
        return filter
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    open fun securityEvaluationContextExtension(): EvaluationContextExtensionSupport {
        return SecurityEvaluationContextExtension()
    }
}
