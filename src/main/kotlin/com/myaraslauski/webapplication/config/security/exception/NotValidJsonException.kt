package com.myaraslauski.webapplication.config.security.exception

import org.springframework.security.core.AuthenticationException

class NotValidJsonException(t: Throwable) : AuthenticationException("Content does not conform to JSON syntax", t)
