package com.myaraslauski.webapplication.config.security.exception

import org.springframework.security.core.AuthenticationException


class InvalidJwtTokenException : AuthenticationException {

    constructor(t: Throwable) : super("Invalid JWT Token", t)

    constructor(msg: String, t: Throwable) : super(msg, t)
}