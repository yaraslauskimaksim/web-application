package com.myaraslauski.webapplication.web.controller

import com.myaraslauski.webapplication.service.security.UserService
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseStatus

@Controller
class LogoutController(private val userUIDService: UserService) {

    @GetMapping("/api/logout")
    @ResponseStatus(HttpStatus.OK)
    fun logout() {
        val authentication = SecurityContextHolder.getContext().authentication
        val username = authentication.name
        userUIDService.removeTokenUIDFor(username)
    }
}
