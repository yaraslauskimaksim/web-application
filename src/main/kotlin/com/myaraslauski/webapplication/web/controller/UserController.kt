package com.myaraslauski.webapplication.web.controller

import com.myaraslauski.webapplication.model.user.UserProfile
import com.myaraslauski.webapplication.service.security.UserService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class UserController(private val userService: UserService) {
    @PostMapping(path = ["/api/internal/updateUser"])
    fun updateProfile(@Valid @RequestBody userProfile: UserProfile) = userService.updateProfile(userProfile)

    @PostMapping(path = ["/resetPassword"])
    fun resetPassword(@RequestParam("emailOrLogin") emailOrLogin: String) =
        userService.resetPassword(emailOrLogin.toLowerCase())
}
