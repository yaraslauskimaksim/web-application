package com.myaraslauski.webapplication.web.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody

@BasePathAwareController
class HelloController(@Value("\${text.fill.hello}") val message: String) {


    @GetMapping("/hello")
    @ResponseBody
    fun hello() = message
}