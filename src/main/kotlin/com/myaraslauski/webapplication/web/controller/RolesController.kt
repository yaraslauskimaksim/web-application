package com.myaraslauski.webapplication.web.controller


import com.myaraslauski.webapplication.model.security.Role
import com.myaraslauski.webapplication.service.admin.AdminService
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody

@BasePathAwareController
class RolesController(private val adminService: AdminService) {

    @GetMapping("/roles")
    @ResponseBody
    fun roles(): List<Role> = adminService.listRoles()
}
