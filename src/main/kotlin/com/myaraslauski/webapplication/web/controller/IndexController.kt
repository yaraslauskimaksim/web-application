package com.myaraslauski.webapplication.web.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class IndexController(@Value("\${server.domain}") private val domain: String,
                      @Value("\${server.port}") private val port: Int) {

    @GetMapping("/")
    fun index() = ModelAndView("index", "baseUrl", "$domain:$port")
}