package com.myaraslauski.webapplication.web.handler

import com.fasterxml.jackson.databind.ObjectMapper
import com.myaraslauski.webapplication.config.security.exception.ErrorResponse
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletResponse

@Component
class ErrorHandler(private val objectMapper: ObjectMapper) {

    fun handle(response: HttpServletResponse, e: Exception) {
        val errorResponse = ErrorResponse(e)

        response.status = errorResponse.status
        response.contentType = MediaType.APPLICATION_JSON_VALUE

        objectMapper.writeValue(response.writer, errorResponse)
    }
}
