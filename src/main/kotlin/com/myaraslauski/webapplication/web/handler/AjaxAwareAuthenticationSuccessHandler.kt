package com.myaraslauski.webapplication.web.handler

import com.fasterxml.jackson.databind.ObjectMapper
import com.myaraslauski.webapplication.component.security.JwtTokenFactory
import com.myaraslauski.webapplication.service.security.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.WebAttributes
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.stereotype.Component
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AjaxAwareAuthenticationSuccessHandler(
        private val objectMapper: ObjectMapper,
        private val tokenFactory: JwtTokenFactory,
        private val userUIDService: UserService
) : AuthenticationSuccessHandler {

    override fun onAuthenticationSuccess(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authentication: Authentication
    ) {
        val userDetails = UserDetails::class.java.cast(authentication.principal)

        val uid = UUID.randomUUID().toString()

        userUIDService.saveRefreshTokenUID(userDetails.username, uid)

        val accessToken = tokenFactory.createAccessJwtToken(userDetails)
        val refreshToken = tokenFactory.createRefreshToken(userDetails, uid)

        val tokenMap = mapOf(
            "token" to accessToken.getToken(),
            "refreshToken" to refreshToken.getToken()
        )

        response.status = HttpStatus.OK.value()
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        objectMapper.writeValue(response.writer, tokenMap)

        clearAuthenticationAttributes(request)
    }

    private fun clearAuthenticationAttributes(request: HttpServletRequest) {
        request.getSession(false)?.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)
    }
}
