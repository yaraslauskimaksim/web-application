package com.myaraslauski.webapplication.web.filter

import com.fasterxml.jackson.databind.ObjectMapper
import com.itsupportme.datawarehouse.config.security.exception.AuthMethodNotSupportedException
import com.myaraslauski.webapplication.config.security.exception.NotValidJsonException
import com.myaraslauski.webapplication.config.security.exception.RequiredFieldEmptyException
import com.myaraslauski.webapplication.model.security.LoginRequest
import com.myaraslauski.webapplication.web.handler.ErrorHandler
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AjaxLoginProcessingFilter(
        defaultProcessUrl: String,
        private val successAuthHandler: AuthenticationSuccessHandler,
        private val errorHandler: ErrorHandler,
        private val objectMapper: ObjectMapper
) : AbstractAuthenticationProcessingFilter(defaultProcessUrl) {
    private val log = LoggerFactory.getLogger(javaClass)

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        if (HttpMethod.POST.name != request.method) {
            log.debug("Authentication method not supported. Request method: {}", request.method)
            throw AuthMethodNotSupportedException("Authentication method not supported")
        }

        val loginRequest = try {
            objectMapper.readValue(request.reader, LoginRequest::class.java)
        } catch (ex: Exception) {
            throw NotValidJsonException(ex)
        }

        if (loginRequest.username.isBlank() || loginRequest.password.isBlank()) {
            throw RequiredFieldEmptyException("Username or Password not provided", Throwable())
        }

        val token = UsernamePasswordAuthenticationToken(loginRequest.username, loginRequest.password)
        return authenticationManager.authenticate(token)
    }

    public override fun successfulAuthentication(
        request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain?,
        authResult: Authentication
    ) {
        successAuthHandler.onAuthenticationSuccess(request, response, authResult)
    }

    public override fun unsuccessfulAuthentication(
        request: HttpServletRequest, response: HttpServletResponse,
        failed: AuthenticationException
    ) {
        SecurityContextHolder.clearContext()
        errorHandler.handle(response, failed)
    }
}
