package com.myaraslauski.webapplication.web.filter

import com.myaraslauski.webapplication.component.security.extractor.TokenExtractor
import com.myaraslauski.webapplication.model.security.JwtAuthenticationToken
import com.myaraslauski.webapplication.model.security.RawAccessJwtToken
import com.myaraslauski.webapplication.web.handler.ErrorHandler
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtTokenAuthenticationProcessingFilter(
        private val errorHandler: ErrorHandler,
        private val tokenExtractor: TokenExtractor,
        matcher: RequestMatcher
) : AbstractAuthenticationProcessingFilter(matcher) {

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val tokenPayload = request.getHeader(JWT_TOKEN_HEADER_PARAM)
        val token = if (tokenPayload == null) {
            RawAccessJwtToken(request.getParameter("token"))
        } else {
            RawAccessJwtToken(tokenExtractor.extract(tokenPayload))
        }
        return authenticationManager.authenticate(JwtAuthenticationToken(token))
    }

    public override fun successfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain,
        authResult: Authentication
    ) {
        val context = SecurityContextHolder.createEmptyContext()
        context.authentication = authResult
        SecurityContextHolder.setContext(context)
        chain.doFilter(request, response)
    }

    public override fun unsuccessfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        failed: AuthenticationException
    ) {
        SecurityContextHolder.clearContext()
        errorHandler.handle(response, failed)
    }

    companion object {
        private const val JWT_TOKEN_HEADER_PARAM = "X-Authorization"
    }
}
