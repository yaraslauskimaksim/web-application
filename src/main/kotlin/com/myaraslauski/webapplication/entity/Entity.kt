package com.myaraslauski.webapplication.entity

import java.time.LocalDateTime

interface Entity<out L> {
    val id: L?

    val userAdded: User?

    val dateAdded: LocalDateTime?

    val userModified: User?

    val dateModified: LocalDateTime?
}
