package com.myaraslauski.webapplication.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.myaraslauski.webapplication.model.security.Role
import org.hibernate.validator.constraints.Email
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import javax.persistence.AssociationOverride
import javax.persistence.AssociationOverrides
import javax.persistence.AttributeOverride
import javax.persistence.AttributeOverrides
import javax.persistence.CollectionTable
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.JoinColumn
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Entity
@Table(
    name = "may_users",
    uniqueConstraints = [UniqueConstraint(columnNames = ["Usr_Username"]), UniqueConstraint(columnNames = ["Usr_Ref_UID"])]
)
@AssociationOverrides(
    AssociationOverride(name = "userAdded", joinColumns = [JoinColumn(name = "Usr_Usr_Added")]),
    AssociationOverride(name = "userModified", joinColumns = [JoinColumn(name = "Usr_Usr_Modified")])
)
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "Usr_Id")),
    AttributeOverride(name = "dateAdded", column = Column(name = "Usr_Date_Added", columnDefinition = "DATETIME")),
    AttributeOverride(name = "dateModified", column = Column(name = "Usr_Date_Modified", columnDefinition = "DATETIME"))
)
class User : AbstractEntity<Long>() {

    companion object {
        private val PASSWORD_ENCODER = BCryptPasswordEncoder()
    }

    @Size(min = 3, max = 45)
    @Column(name = "Usr_Username", length = 45, unique = true)
    var username = ""
        set(value) {
            field = value.toLowerCase()
        }

    @Column(name = "Usr_Password", length = 60)
    var password = ""
        set(value) {
            field = PASSWORD_ENCODER.encode(value)
        }

    @Column(name = "Usr_Is_Enabled")
    var enabled = false

    @Pattern(regexp = "^[A-Z][a-z]{1,29}$", message = "First letter must be capital. Size must be between 2 and 30 ")
    @Column(name = "Usr_First", length = 30)
    var first = ""

    @Pattern(regexp = "^[A-Z][a-z]{1,29}$", message = "First letter must be capital. Size must be between 2 and 30 ")
    @Column(name = "Usr_Last", length = 30)
    var last = ""

    @Email(message = "Invalid email")
    @Column(name = "Usr_Email", length = 128)
    var email = ""
        set(value) {
            field = value.toLowerCase()
        }

    @Column(name = "Usr_Ref_UID", length = 36)
    @JsonIgnore
    var refreshTokenUID: String? = null

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @CollectionTable(name = "may_user_role", joinColumns = [JoinColumn(name = "Ur_Usr_Id")])
    @Column(name = "Ur_Role")
    var roles = mutableSetOf<Role>()
}
