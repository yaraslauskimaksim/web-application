package com.myaraslauski.webapplication.entity

import org.apache.commons.lang3.builder.HashCodeBuilder
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractEntity<L> : Entity<L> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: L? = null

    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    override var userAdded: User? = null

    @CreatedDate
    override var dateAdded: LocalDateTime? = null

    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    override var userModified: User? = null

    @LastModifiedDate
    override var dateModified: LocalDateTime? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        @Suppress("UNCHECKED_CAST")
        return identityEquals(other as Entity<L>)
    }

    private fun identityEquals(other: Entity<L>): Boolean {
        if (id == null) {
            return false
        }
        return id == other.id
    }

    override fun hashCode() = HashCodeBuilder().append(id).toHashCode()
}
